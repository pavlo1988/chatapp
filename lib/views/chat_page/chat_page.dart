import 'package:chatapp/values/color.dart';
import 'package:chatapp/values/text_style.dart';
import 'package:chatapp/views/chat_page/chat_detail_page.dart';
import 'package:flutter/material.dart';

import '../../models/chat_user.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<ChatUser> chatUsers = [
    ChatUser(
        name: "Olivia Carr",
        photoUrl: "assets/images/profile.jpg",
        messageText: "Till Tomorrow",
        messageType: "text",
        unreadMessages: 1),
    ChatUser(
        name: "Amber Howard",
        photoUrl: "assets/images/profile1.jpg",
        messageText:
            "😂😂 this is the long text to test the overflow this is the long text to test the overflow",
        messageType: "text",
        unreadMessages: 4),
    ChatUser(
      name: "Randal Charles",
      photoUrl: "assets/images/profile2.jpg",
      messageText: "don't know saturday",
      messageType: "text",
    ),
    ChatUser(
        name: "Louise Bennett",
        photoUrl: "assets/images/profile3.jpg",
        messageText: "",
        messageType: "voice",
        unreadMessages: 3),
    ChatUser(
        name: "Yana Honchar",
        photoUrl: "assets/images/profile4.jpg",
        messageText: "",
        messageType: "Nice to meet you",
        unreadMessages: 2),
    ChatUser(
        name: "Anastasiia Moisiesieva",
        photoUrl: "assets/images/profile5.jpg",
        messageText: "",
        messageType: "Nice to meet you",
        unreadMessages: 2),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: const Text("Chats", style: AppTextStyle.pageTitle),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.only(top: 20.0, right: 16.0, left: 16.0),
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                hintText: "Search",
                hintStyle: const TextStyle(color: Colors.grey),
                prefixIcon: const Icon(
                  Icons.search,
                  color: Colors.black,
                  size: 20,
                ),
                filled: true,
                fillColor: AppColors.textFieldColor,
                contentPadding: const EdgeInsets.all(8),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade100)),
              ),
            ),
            const SizedBox(height: 16.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "All chats",
                  style: TextStyle(fontSize: 20.0),
                ),
                IconButton(
                  icon: const Icon(Icons.tune),
                  onPressed: () {},
                ),
              ],
            ),
            Expanded(
                child: ListView.builder(
                    itemCount: chatUsers.length,
                    itemBuilder: (context, int index) =>
                        chatChannel(context, chatUsers[index]))),
          ],
        ),
      ),
    );
  }
}

Widget chatChannel(BuildContext context, ChatUser chatUser) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatDetailPage(chatUser: chatUser)));
    },
    child: Container(
      color: AppColors.secondaryColor,
      padding: const EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
      margin: const EdgeInsets.only(bottom: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: AssetImage(chatUser.photoUrl),
                  maxRadius: 30,
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Container(
                    color: Colors.transparent,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          chatUser.name,
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          chatUser.messageType == "text"
                              ? chatUser.messageText
                              : "Voice Message",
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 13, color: Colors.grey.shade600),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(width: 8.0),
          chatUser.unreadMessages != null
              ? Container(
                  decoration: const BoxDecoration(
                    color: AppColors.primaryColor,
                    shape: BoxShape.circle,
                  ),
                  height: 20,
                  width: 20,
                  child: Center(
                      child: Text(
                    chatUser.unreadMessages.toString(),
                    style: const TextStyle(color: Colors.white, fontSize: 12.0),
                  )),
                )
              : const SizedBox(height: 20, width: 20),
        ],
      ),
    ),
  );
}
