import 'package:chatapp/models/chat_user.dart';
import 'package:chatapp/values/color.dart';
import 'package:flutter/material.dart';

import '../../models/chat_message.dart';
import '../../values/text_style.dart';

class ChatDetailPage extends StatefulWidget {
  final ChatUser chatUser;
  const ChatDetailPage({required this.chatUser, Key? key}) : super(key: key);

  @override
  State<ChatDetailPage> createState() => _ChatDetailPageState();
}

class _ChatDetailPageState extends State<ChatDetailPage> {
  List<ChatMessage> messages = [
    ChatMessage(
      messageContent:
          "How do you feel? I'd like to suggest to meet tomorrow and discuss everything in details.",
      time: "12:34",
      isMyMessage: true,
    ),
    ChatMessage(
        messageContent: "Good idea... what do you think about these photos?",
        attachedImages: [
          "assets/images/profile5.jpg",
          "assets/images/profile6.jpg",
        ],
        time: "14:20",
        isMyMessage: false),
    ChatMessage(
        messageContent: "they are wonderful... 😂😂😂",
        time: "12:34",
        isMyMessage: true),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(widget.chatUser.name, style: AppTextStyle.pageTitle),
          centerTitle: true,
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.arrow_back_ios, color: Colors.black),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: CircleAvatar(
                backgroundImage: AssetImage(
                  widget.chatUser.photoUrl,
                ),
                maxRadius: 25,
              ),
            ),
          ],
        ),
        body: Column(
          children: [
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                child: ListView.builder(
                    itemCount: messages.length,
                    itemBuilder: (BuildContext context, int index) =>
                        messageTile(context,
                            message: messages[index],
                            chatUser: widget.chatUser)),
              ),
            ),
            _buildTextField()
          ],
        ));
  }
}

Widget messageTile(BuildContext context,
    {required ChatMessage message, required ChatUser chatUser}) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 16.0),
    child: Row(
      mainAxisAlignment:
          message.isMyMessage ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: [
        Container(
            color: message.isMyMessage
                ? AppColors.primaryColor
                : AppColors.secondaryColor,
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width * 0.8,
            ),
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      message.isMyMessage ? 'You' : chatUser.name,
                      style: TextStyle(
                        color: message.isMyMessage
                            ? Colors.white.withOpacity(0.6)
                            : Colors.grey,
                      ),
                    ),
                    Text(
                      message.time,
                      style: TextStyle(
                          color: message.isMyMessage
                              ? Colors.white.withOpacity(0.6)
                              : Colors.grey),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),
                Text(
                  message.messageContent,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: message.isMyMessage ? Colors.white : Colors.black,
                  ),
                ),
                if (message.attachedImages != null &&
                    message.attachedImages!.isNotEmpty)
                  SizedBox(
                    height: (MediaQuery.of(context).size.width * 0.4 - 10) *
                        (message.attachedImages!.length / 2).ceil(),
                    child: GridView.count(
                      crossAxisCount: 2,
                      physics: const NeverScrollableScrollPhysics(),
                      children: message.attachedImages!
                          .map((attachedImage) => Padding(
                                padding: const EdgeInsets.all(8),
                                child: Image.asset(
                                  attachedImage,
                                ),
                              ))
                          .toList(),
                    ),
                  )
              ],
            )),
      ],
    ),
  );
}

Widget _buildTextField() {
  return Container(
    padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
    color: AppColors.textFieldColor,
    child: Flex(
      direction: Axis.horizontal,
      children: <Widget>[
        Expanded(
          child: Column(
            children: [
              Container(
                constraints: const BoxConstraints(maxHeight: 125),
                child: const TextField(
                  maxLines: null,
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                      hintText: "Write message",
                      hintStyle: TextStyle(color: Colors.black54),
                      border: InputBorder.none),
                ),
              ),
              Row(
                children: [
                  GestureDetector(
                      onTap: () {},
                      child: const Icon(
                        Icons.keyboard_voice_outlined,
                        color: Colors.grey,
                      )),
                  const SizedBox(width: 16.0),
                  GestureDetector(
                      onTap: () {},
                      child: const Icon(Icons.photo_camera_outlined,
                          color: Colors.grey)),
                  const SizedBox(width: 16.0),
                  GestureDetector(
                      onTap: () {},
                      child:
                          const Icon(Icons.photo_outlined, color: Colors.grey)),
                ],
              ),
            ],
          ),
        ),
        const SizedBox(
          width: 15,
        ),
        SizedBox(
          height: 35,
          width: 35,
          child: FloatingActionButton(
            onPressed: () {},
            child: const Icon(
              Icons.send,
              color: Colors.white,
              size: 16,
            ),
            backgroundColor: Colors.black,
            elevation: 0,
          ),
        ),
      ],
    ),
  );
}
