class ChatMessage {
  String messageContent;
  String time;
  bool isMyMessage;
  List<String>? attachedImages;

  ChatMessage({
    required this.messageContent,
    required this.time,
    required this.isMyMessage,
    this.attachedImages,
  });
}
