class ChatUser {
  String name;
  String photoUrl;
  String messageText;
  int? unreadMessages;
  String messageType;
  ChatUser(
      {required this.name,
      required this.photoUrl,
      required this.messageText,
      this.unreadMessages,
      required this.messageType});
}
