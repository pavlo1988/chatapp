import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xff7166f9);
  static const Color secondaryColor = Color(0xfff6f7fc);
  static const Color textFieldColor = Color(0xfff8f8f9);
}
