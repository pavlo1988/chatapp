import 'package:chatapp/views/mainPage/mainPage.dart';
import 'package:flutter/material.dart';

class OnboardingPage extends StatefulWidget {
  const OnboardingPage({Key? key}) : super(key: key);

  @override
  State<OnboardingPage> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        minimum: const EdgeInsets.only(top: 30, bottom: 30),
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Text(
                  "Hi Ricki",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                const Text(
                  "Welcome back",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.normal,
                      color: Colors.grey),
                ),
                const SizedBox(height: 36),
                Image.asset("assets/images/onboarding.jpg"),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "You have unread letters",
                        style: TextStyle(color: Colors.grey),
                      ),
                      const Text("12",
                          style: TextStyle(
                              fontSize: 96, fontWeight: FontWeight.w100)),
                      const SizedBox(height: 30),
                      FloatingActionButton(
                        backgroundColor: Colors.black,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const MainPage()));
                        },
                        child: const Icon(Icons.arrow_forward_ios),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
